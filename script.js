function division(n1 = 1, n2 = 1) {
    result = n1 / n2;
    console.log(result);
}

do {
    firstNumber = parseFloat(prompt("Введіть перше число."));
    secondNumber = parseFloat(prompt("Введіть друге число"));

}   while (isNaN(firstNumber) || isNaN(secondNumber))

division(firstNumber, secondNumber);


///////////////////////////////////////////////////////////////////////////////////


let operationAsk = prompt("Виберіть операцію: +, -, *, або /")

function operations(a, b) {
    switch (operationAsk) {
        case "+":
            operationResult = a + b;
            break;
        case "-":
            operationResult = a - b;
            break;
        case "*":
            operationResult = a * b;
            break;
        case "/":
            if (b === 0) {
                alert("Не можливо ділити на 0");
            } else {
                operationResult = a / b;
            }
            break;

        default:
            alert("Такої операції не існує")
    }
    return;
}

do {
    firstValue = parseFloat(prompt("Введіть перше число."));
    secondValue = parseFloat(prompt("Введіть друге число"));

}   while (isNaN(firstValue) || isNaN(secondValue))

const outcome = operations(firstValue, secondValue);
console.log(`Результатом операції є: ${operationResult}`);


///////////////////////////////////////////////////////////////////////////////////


function factorial(factorial) {
    let fvar = 1;
    for (i = 1; i <= factorial; i++) {
        fvar = fvar * i;
    };
    alert(`Факторіалом введеного числа є - ${fvar}`);
    return;
}

factorial(parseFloat(prompt("Введіть число.")));